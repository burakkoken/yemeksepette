﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class SepetimController : KullaniciController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        // GET: Sepetim
        public ActionResult Index(bool? sepetOnaylandiMi)
        {
            if (sepetOnaylandiMi != null)
            {
                ViewBag.SepetOnayMesaj = sepetOnaylandiMi;
            }

            var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

            var sepetListesi = db.Siparisler.Include("Sube").Include("Urun")
                .Where(x => x.MusteriID == kullaniciBilgi.MusteriID && x.OnaylanmisMi == false).ToList()
                .GroupBy(x => x.SubeID);

            var toplamTutar = 0f;
            foreach (var group in sepetListesi)
            {
                foreach (var siparis in group)
                {
                    toplamTutar += siparis.Adet * siparis.Urun.Fiyat;
                }
            }

            ViewBag.ToplamTutar = toplamTutar;

            return View(sepetListesi);
        }

        public ActionResult SepettenCikar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Sepetim");
            }


            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

                var siparis = db.Siparisler.Where(x => x.SiparisID == id && x.MusteriID == kullaniciBilgi.MusteriID).First();
                if (siparis != null)
                {
                    db.Siparisler.Remove(siparis);
                    db.SaveChanges();
                }

            }

            return RedirectToAction("Index", "Sepetim");
        }

        public ActionResult SepetiOnayla()
        {
            var sepetOnay = false;
            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {

                var kullanici = applicationDbContext.Users.
                    Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

                var sepetListesi = db.Siparisler.Include("Sube").Include("Urun")
                    .Where(x => x.MusteriID == kullaniciBilgi.MusteriID && x.OnaylanmisMi == false).ToList();
                sepetListesi.ForEach(x => x.OnaylanmisMi = true);
                db.SaveChanges();
                sepetOnay = true;
            }

            return RedirectToAction("Index", "Sepetim", new { sepetOnaylandiMi = sepetOnay });

        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}