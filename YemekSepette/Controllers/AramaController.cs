﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class AramaController : Controller
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        // GET: Arama
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult AramaSonuc(RestoranArama ra)
        {
            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();

                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);
                if (userManager.IsInRole(kullanici.Id, "Kullanıcı") == false)
                {
                    bool adminRolundeMi = userManager.IsInRole(kullanici.Id, "Admin");
                    bool restoranRolundeMi = userManager.IsInRole(kullanici.Id, "Restoran");
                    bool subeRolundeMi = userManager.IsInRole(kullanici.Id, "Şube");
                    if (adminRolundeMi)
                    {
                        return Redirect("/Admin");
                    }
                    else if (restoranRolundeMi)
                    {
                        return Redirect("/RestoranPanel");
                    }
                    else if (subeRolundeMi)
                    {
                        return Redirect("/SubePanel");
                    }
                }

            }


            ViewBag.AramaIcerik = ra.Icerik;

            if (ModelState.IsValid)
            {
                /* cookie'yi olustur */
                HttpCookie sehirIDCookie = new HttpCookie("SehirID", ra.SehirID.ToString());
                HttpContext.Request.Cookies.Remove("SehirID");
                HttpContext.Response.Cookies.Add(sehirIDCookie);

                var restoranlar = db.Subeler.Include("Restoran").Where(x => x.SemtID == ra.SemtID && x.Semt.SehirID == ra.SehirID);
                /* eger arama icerigi varsa */
                if (ra.Icerik != null)
                {
                    restoranlar = restoranlar.Where(x => x.SubeIsmi.Contains(ra.Icerik));
                }

                return View(restoranlar);
            }

            return View();
        }

        [ChildActionOnly]
        public ActionResult _Arama()
        {
            /* sehir listesi */
            var sehirler = db.Subeler.Select(x =>
            new { SehirAd = x.Semt.Sehir.SehirAd, SehirID = x.Semt.SehirID }).Distinct().ToList();

            //var sehirler = subeler.Select(x => new Sehir { SehirAd = x.);
            //ViewBag.SehirListesi = db.Sehirler;
            ViewBag.SehirListesi = sehirler;
            /* sehir id cookie kontrolu - eger varsa */
            if (HttpContext.Request.Cookies["SehirID"] != null)
            {
                int sehirID = Convert.ToInt16(HttpContext.Request.Cookies["SehirID"].Value); ;
                ViewBag.SehirIDCookie = sehirID;
                ViewBag.SemtListesi = db.Semtler.Where(x => x.Sehir.SehirID == sehirID);
            }


            return View();
        }

        public JsonResult SemtleriDoldur(int SehirID)
        {
            ICollection<Semt> semtler = db.Semtler.Where(s => s.SehirID == SehirID).ToList();
            var data = semtler.Select(s => new
            {
                SemtID = s.SemtID,
                SemtAd = s.SemtAd
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}