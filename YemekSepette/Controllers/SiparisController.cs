﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class SiparisController : Controller
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        // GET: Siparis
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult RestoranSube(int? id)
        {
            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();

                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);
                if (userManager.IsInRole(kullanici.Id, "Kullanıcı") == false)
                {
                    bool adminRolundeMi = userManager.IsInRole(kullanici.Id, "Admin");
                    bool restoranRolundeMi = userManager.IsInRole(kullanici.Id, "Restoran");
                    bool subeRolundeMi = userManager.IsInRole(kullanici.Id, "Şube");
                    if (adminRolundeMi)
                    {
                        return Redirect("/Admin");
                    }
                    else if (restoranRolundeMi)
                    {
                        return Redirect("/RestoranPanel");
                    }
                    else if (subeRolundeMi)
                    {
                        return Redirect("/SubePanel");
                    }
                }

            }

            if (id == null)
            {
                return HttpNotFound();
            }

            var sube = db.Subeler.Where(x => x.SubeID == id).FirstOrDefault();

            if (sube == null)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.SubeIsmi = sube.SubeIsmi;
            ViewBag.SubeYorumSayisi = db.SubeYorumlari.Where(x => x.SubeID == id).Count();
            ViewBag.ID = id;

            return View();
        }

        [ChildActionOnly]
        public ActionResult _SubeBilgi(int id)
        {
            var SubeBilgi = db.Subeler.First(x => x.SubeID == id);
            return View(SubeBilgi);
        }

        [ChildActionOnly]
        public ActionResult _SubeUrunler(int id)
        {
            var SubeUrunler = db.Menuler.Where(x => x.SubeID == id).ToList();
            return View(SubeUrunler);
        }


        public ActionResult _SubeYapilanYorumlar(int id, int? page)
        {
            var SubeYapilanYorumlar = db.SubeYorumlari.Include("Musteri").Where(x => x.SubeID == id).OrderByDescending(x => x.Tarih);
            ViewBag.SubeID = id;
            int pageSize = 5;
            int pageNumber = (page ?? 1);


            return View(SubeYapilanYorumlar.ToPagedList(pageNumber, pageSize));
        }

        [ChildActionOnly]
        public ActionResult _SubeYorumYap(int subeId)
        {
            ViewBag.SubeID = subeId;
            return View();
        }

        [HttpPost]
        public ActionResult SubeYorumYap(SubeYorum subeYorum)
        {
            var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

            subeYorum.Tarih = DateTime.Now;
            subeYorum.MusteriID = kullaniciBilgi.MusteriID;
            db.SubeYorumlari.Add(subeYorum);
            db.SaveChanges();

            return RedirectToAction("RestoranSube", "Siparis", new { id = subeYorum.SubeID });
        }

        [ChildActionOnly]
        public ActionResult _SubeSorunBildir(int subeId)
        {
            ViewBag.SubeID = subeId;
            return View();
        }

        [HttpPost]
        public ActionResult SubeSorunBildir(SubeSorun subeSorun)
        {
            var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

            subeSorun.Tarih = DateTime.Now;
            subeSorun.MusteriID = kullaniciBilgi.MusteriID;
            db.SubeSorunlari.Add(subeSorun);
            db.SaveChanges();

            return RedirectToAction("RestoranSube", "Siparis", new { id = subeSorun.SubeID });
        }

        public JsonResult OturumDurumunuKontrolEt(int? urunId)
        {

            if (urunId == null)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            var urun = db.Urunler.Where(x => x.UrunID == urunId).First();

            DateTime simdi = DateTime.Now;
            string baslangic = String.Format("{0:HH:mm}", urun.Menu.Sube.CalismaBaslangicSaati);
            string bitis = String.Format("{0:HH:mm}", urun.Menu.Sube.CalismaBitisSaati);
            DateTime baslangicSaati = DateTime.Parse(baslangic);
            DateTime bitisSaati = DateTime.Parse(bitis);

            var calismaDurumu = true;
            if (DateTime.Compare(simdi, baslangicSaati) < 0 || DateTime.Compare(simdi, bitisSaati) > 0)
            {
                calismaDurumu = false;
            }

            var data = new
            {
                OturumAcilmisMi = AuthenticationManager.User.Identity.IsAuthenticated,
                SubeDurumu = calismaDurumu
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SiparisSayilariniAl()
        {
            if (AuthenticationManager.User.Identity.IsAuthenticated == false)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();
            var siparisSayisi = db.Siparisler.Where(x => x.MusteriID == kullaniciBilgi.MusteriID && x.OnaylanmisMi == false).Count();
            var bekleyenSayisi = db.Siparisler.Where(x => x.MusteriID == kullaniciBilgi.MusteriID && x.OnaylanmisMi == true && x.IletildiMi == false).Count();

            var data = new
            {
                SiparisSayisi = siparisSayisi,
                BekleyenSiparisSayisi = bekleyenSayisi
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SepeteEkle(int? urunId, int? urunAdet)
        {
            var data = false;


            if (urunId == null || urunAdet == null)
            {
                return Json(data, JsonRequestBehavior.AllowGet);
            }

            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();
                var urun = db.Urunler.Where(x => x.UrunID == urunId).First();

                Siparis yeniSiparis = new Siparis
                {
                    UrunID = (int)urunId,
                    Adet = (int)urunAdet,
                    MusteriID = kullaniciBilgi.MusteriID,
                    SubeID = urun.Menu.SubeID,
                    OnaylanmisMi = false,
                    IletildiMi = false
                };
                db.Siparisler.Add(yeniSiparis);
                db.SaveChanges();
                data = true;
                // urunid, musteriid, sube id, adet
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}