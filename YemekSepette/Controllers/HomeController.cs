﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class HomeController : Controller
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        public ActionResult Index()
        {
            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();

                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);
                if (userManager.IsInRole(kullanici.Id, "Kullanıcı") == false)
                {
                    bool adminRolundeMi = userManager.IsInRole(kullanici.Id, "Admin");
                    bool restoranRolundeMi = userManager.IsInRole(kullanici.Id, "Restoran");
                    bool subeRolundeMi = userManager.IsInRole(kullanici.Id, "Şube");
                    if (adminRolundeMi)
                    {
                        return Redirect("/Admin");
                    }
                    else if (restoranRolundeMi)
                    {
                        return Redirect("/RestoranPanel");
                    }
                    else if (subeRolundeMi)
                    {
                        return Redirect("/SubePanel");
                    }
                }

            }

            return View();
        }

        [ChildActionOnly]
        public ActionResult _OnerilenRestoranlar()
        {
            var onerilenRestoranlar = db.OnerilenRestoranlar.OrderBy(x => x.Sira).ToList();
            return View(onerilenRestoranlar);
        }

        [ChildActionOnly]
        public ActionResult _OturumAc()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult _KullaniciMenu()
        {
            var kullanici = applicationDbContext.Users.
               Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();
            ViewBag.KullaniciAd = kullaniciBilgi.AdSoyad;

            ViewBag.SepettekiSiparisSayisi = db.Siparisler.Where(x => x.OnaylanmisMi == false && x.MusteriID == kullaniciBilgi.MusteriID).Count();
            ViewBag.BekleyenSiparisSayisi = db.Siparisler.Where(x => x.OnaylanmisMi == true && x.IletildiMi == false && x.MusteriID == kullaniciBilgi.MusteriID).Count();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}