﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class BekleyenSiparislerController : KullaniciController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();


        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Musteriler.Where(x => x.MusteriID == kullanici.KullaniciTipId).First();

            var sepetListesi = db.Siparisler.Include("Sube").Include("Urun")
                .Where(x => x.MusteriID == kullaniciBilgi.MusteriID && x.OnaylanmisMi == true && x.IletildiMi == false).ToList()
                .GroupBy(x => x.SubeID);

            return View(sepetListesi);
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}