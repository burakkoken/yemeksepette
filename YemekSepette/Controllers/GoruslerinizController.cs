﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Controllers
{
    public class GoruslerinizController : Controller
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        // GET: Gorusleriniz
        public ActionResult Index(int? page)
        {
            if (AuthenticationManager.User.Identity.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();

                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);
                if (userManager.IsInRole(kullanici.Id, "Kullanıcı") == false)
                {
                    bool adminRolundeMi = userManager.IsInRole(kullanici.Id, "Admin");
                    bool restoranRolundeMi = userManager.IsInRole(kullanici.Id, "Restoran");
                    bool subeRolundeMi = userManager.IsInRole(kullanici.Id, "Şube");
                    if (adminRolundeMi)
                    {
                        return Redirect("/Admin");
                    }
                    else if (restoranRolundeMi)
                    {
                        return Redirect("/RestoranPanel");
                    }
                    else if (subeRolundeMi)
                    {
                        return Redirect("/SubePanel");
                    }
                }

            }

            var gorusler = db.Gorusler.OrderByDescending(x => x.GorusTarih);
            ViewBag.GorusSayisi = gorusler.Count();
            ViewBag.GorusSayfasindaMiyiz = true;

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(gorusler.ToPagedList(pageNumber, pageSize));
        }

        [ChildActionOnly]
        public ActionResult _Gorusler()
        {
            var gorusler = db.Gorusler.OrderByDescending(x => x.GorusTarih);
            return View(gorusler);
        }

        [ChildActionOnly]
        public ActionResult _GorusBildir()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _GorusBildir(Gorus gorus)
        {
            if (ModelState.IsValid)
            {
                gorus.GorusTarih = DateTime.Now;
                db.Gorusler.Add(gorus);
                db.SaveChanges();
            }


            return RedirectToAction("Index");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}