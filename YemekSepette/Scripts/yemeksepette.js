﻿function siparisSayilariniAl(lang) {

    $.ajax({
        url: 'http://' + window.location.host + '/' + lang + '/Siparis/SiparisSayilariniAl',
        type: "GET",
        dataType: "JSON",
        data: {},
        success: function (sonuc) {
            if (sonuc == null) {
                return;
            }
            $('#siparisbadge').html(sonuc.SiparisSayisi);
            $('#bekleyenbadge').html(sonuc.BekleyenSiparisSayisi);
        }
    });

}

function SemtleriDoldur(lang) {

    var id = $('#SehirID').val();
    var defaultValue = $("#SemtID option:first-child").html();

    if (id == "") {
        $("#SemtID").html("");
        $("#SemtID").append(
            $('<option></option>').val("").html(defaultValue)
        );
        return;
    }

    $.ajax({
        url: 'http://' + window.location.host + '/' + lang + '/Arama/SemtleriDoldur',
        type: "GET",
        dataType: "JSON",
        data: { SehirID: id },
        success: function (semtler) {
            $("#SemtID").html("");
            $("#SemtID").append(
                $('<option></option>').val("").html(defaultValue)
            );
            $.each(semtler, function (i, semt) {
                $("#SemtID").append(
                    $('<option></option>').val(semt.SemtID).html(semt.SemtAd));
            });
        }
    });
}

function KayitSemtleriDoldur() {

    var id = $('#KayitSehirID').val();
    var defaultValue = $("#KayitSemtID option:first-child").html();

    if (id == "") {
        $("#KayitSemtID").html("");
        $("#KayitSemtID").append(
            $('<option></option>').val("").html(defaultValue)
        );
        return;
    }

    $.ajax({
        url: '/Arama/SemtleriDoldur',
        type: "GET",
        dataType: "JSON",
        data: { SehirID: id },
        success: function (semtler) {
            $("#KayitSemtID").html("");
            $("#KayitSemtID").append(
                $('<option></option>').val("").html(defaultValue)
            );
            $.each(semtler, function (i, semt) {
                $("#KayitSemtID").append(
                    $('<option></option>').val(semt.SemtID).html(semt.SemtAd));
            });
        }
    });
}

function sepeteEklemeIsleminiTamamla(lang, id, adetElement) {

    var adet = $(adetElement).val();
    if (jQuery.isNumeric(adet) == false) {
        $("#urunSepetBasarisizModal").modal('show');
        return;
    }

    $.ajax({
        url: 'http://' + window.location.host + '/' + lang + '/Siparis/SepeteEkle',
        type: "GET",
        dataType: "JSON",
        data: {
            urunId: id,
            urunAdet: adet
        },
        success: function (sonuc) {
            if (sonuc == true) {
                $('#urunSepetBasariModal').modal('show');
                siparisSayilariniAl(lang);
            }
            else {
                $("#urunSepetBasarisizModal").modal('show');
            }
        }
    });
}

function sepeteEkle(lang, id, adetElement) {

    $.ajax({
        url: 'http://' + window.location.host + '/' + lang + '/Siparis/OturumDurumunuKontrolEt',
        type: "GET",
        dataType: "JSON",
        data: { urunId: id },
        success: function (sonuclar) {
            if (sonuclar.OturumAcilmisMi == false || sonuclar.SubeDurumu == false) {
                $('#urunSepetMesajModal').modal('show');
            }
            else {
                sepeteEklemeIsleminiTamamla(lang, id, adetElement);
                return;
            }

            if (sonuclar.OturumAcilmisMi == false) {
                $("#urunSepetSubeCalismaSaatleriDisinda").hide();
                $('#urunSepetOturumAcmanizLazim').show();
            }
            else if (sonuclar.SubeDurumu == false) {
                $("#urunSepetSubeCalismaSaatleriDisinda").show();
                $('#urunSepetOturumAcmanizLazim').hide();
            }

        }
    });

}
