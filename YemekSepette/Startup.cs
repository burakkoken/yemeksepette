﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YemekSepette.Startup))]
namespace YemekSepette
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
