﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace YemekSepette
{
    public class LanguageAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string cookieLang = null;
            if (filterContext.HttpContext.Request.Cookies["dil"] != null)
            {
                cookieLang = filterContext.HttpContext.Request.Cookies["dil"].Value;
            }
            string lang = filterContext.RouteData.Values["language"] != null ?
                filterContext.RouteData.Values["language"].ToString() : (cookieLang != null) ? cookieLang : "tr";

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);

            filterContext.HttpContext.Response.Cookies.Add(new HttpCookie("dil") { Value = lang, Expires = DateTime.Now.AddMinutes(1) });

            base.OnActionExecuting(filterContext);
        }
    }
}