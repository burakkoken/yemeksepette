﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using YemekSepette.Models;

namespace YemekSepette.DAL
{
    public class YemekSepetteContext : DbContext
    {
        public YemekSepetteContext() : base("YemekSepetteContext")
        {
        }

        public DbSet<Sehir> Sehirler { get; set; }
        public DbSet<Semt> Semtler { get; set; }

        public DbSet<Restoran> Restoranlar { get; set; }
        public DbSet<Sube> Subeler { get; set; }

        public DbSet<Menu> Menuler { get; set; }
        public DbSet<Urun> Urunler { get; set; }

        public DbSet<SubeYorum> SubeYorumlari { get; set; }
        public DbSet<SubeSorun> SubeSorunlari { get; set; }

        public DbSet<Gorus> Gorusler { get; set; }
        public DbSet<OnerilenRestoran> OnerilenRestoranlar { get; set; }

        public DbSet<Musteri> Musteriler { get; set; }
        public DbSet<Siparis> Siparisler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }

}