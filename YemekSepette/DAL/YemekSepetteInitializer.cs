﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using YemekSepette.Models;

namespace YemekSepette.DAL
{
    public class YemekSepetteInitializer : CreateDatabaseIfNotExists<YemekSepetteContext>
    {
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        protected override void Seed(YemekSepetteContext context)
        {
            #region Sehirler

            var Sehirler = new List<Sehir>
            {
                new Sehir
                {
                    SehirID = 34,
                    SehirAd = "İstanbul"
                },
                new Sehir
                {
                    SehirID = 41,
                    SehirAd = "Kocaeli"
                },
                new Sehir
                {
                    SehirID = 54,
                    SehirAd = "Sakarya"
                }
            };

            Sehirler.ForEach(s => context.Sehirler.Add(s));
            context.SaveChanges();

            #endregion

            #region Semtler

            var Semtler = new List<Semt>
            {
                new Semt
                {
                    SehirID = 41,
                    SemtAd = "Yahyakaptan"
                },
                new Semt
                {
                    SehirID = 41,
                    SemtAd = "İzmit(Alikahya)"
                }
            };

            Semtler.ForEach(s => context.Semtler.Add(s));
            context.SaveChanges();

            #endregion

            #region Restoranlar

            var Restoranlar = new List<Restoran>
            {
                 new Restoran
                 {
                     RestoranIsmi = "Domino's Pizza",
                     RestoranLogo = "/Content/img/restaurant/dominos.png"
                 },
                 new Restoran
                 {
                     RestoranIsmi = "Mc Donalds",
                     RestoranLogo = "/Content/img/restaurant/mcdonalds.png"
                 },
                 new Restoran
                 {
                     RestoranIsmi = "Burger King",
                     RestoranLogo = "/Content/img/restaurant/burgerking.gif"
                 }
            };

            Restoranlar.ForEach(r => context.Restoranlar.Add(r));
            context.SaveChanges();

            #endregion

            #region Subeler

            var Subeler = new List<Sube>
            {
                new Sube
                {
                    RestoranID = 1,
                    SubeIsmi = "Domino's Pizza Yuvam",
                    Telefon = "0262 322 44 00",
                    ServisSuresi = 30,
                    AcikAdres = "Alikahya-Yuvam Akarca No:89",
                    CalismaBaslangicSaati = new DateTime(1970, 1, 1, 11, 0, 0),
                    CalismaBitisSaati = new DateTime(1970, 1, 1, 23, 55, 0),
                    SemtID = 2
                },
                new Sube
                {
                    RestoranID = 2,
                    SubeIsmi = "Mc Donalds Yuvam",
                    Telefon = "0262 311 00 00",
                    ServisSuresi = 40,
                    AcikAdres = "Alikahya-Yuvam Akarca No:24",
                    CalismaBaslangicSaati = new DateTime(1970, 1, 1, 10, 0, 0),
                    CalismaBitisSaati = new DateTime(1970, 1, 1, 2, 0, 0),
                    SemtID = 2
                },
                new Sube
                {
                    RestoranID = 3,
                    SubeIsmi = "Burger King Yahya Kaptan",
                    Telefon = "0262 344 22 22",
                    ServisSuresi = 45,
                    AcikAdres = "Arasta Park AVM Kat:3 No:43",
                    CalismaBaslangicSaati = new DateTime(1970, 1, 1, 11, 0, 0),
                    CalismaBitisSaati = new DateTime(1970, 1, 1, 21, 30, 0),
                    SemtID = 1
                }

            };

            Subeler.ForEach(s => context.Subeler.Add(s));
            context.SaveChanges();

            #endregion

            #region Gorusler

            var Gorusler = new List<Gorus>
            {
                new Gorus
                {
                    GorusIcerik = "Gerçekten çok mükemmel ve kalite bir yemek sipariş sitesi",
                    GorusTarih = new DateTime(2015, 9, 7)
                },
                new Gorus
                {
                    GorusIcerik = "Tebrik ediyorum sizleri",
                    GorusTarih = new DateTime(2015, 10, 28)
                },
                new Gorus
                {
                    GorusIcerik = "Bir teşekkürü gerçekten hakediyorsunuz",
                    GorusTarih = new DateTime(2015, 10, 14)
                },
                new Gorus
                {
                    GorusIcerik = "Birçok yemek sipariş sistemine göre mükemmelsiniz",
                    GorusTarih = new DateTime(2015, 10, 2)
                },
                new Gorus
                {
                    GorusIcerik = "Sorunlarımızla çok çabuk ilgilenmeniz çok müthiş",
                    GorusTarih = new DateTime(2015, 9, 15)
                }
            };

            Gorusler.ForEach(g => context.Gorusler.Add(g));
            context.SaveChanges();
            #endregion

            #region OnerilenRestoranlar

            var OnerilenRestoranlar = new List<OnerilenRestoran>
            {
                new OnerilenRestoran
                {
                    Sira = 1,
                    RestoranID = 3
                },
                new OnerilenRestoran
                {
                    Sira = 2,
                    RestoranID = 2
                },
                new OnerilenRestoran
                {
                    Sira = 3,
                    RestoranID = 1,
                },
                new OnerilenRestoran
                {
                    Sira = 4,
                    RestoranID = 3
                },
                new OnerilenRestoran
                {
                    Sira = 5,
                    RestoranID = 2
                },
                new OnerilenRestoran
                {
                    Sira = 6,
                    RestoranID = 1,
                },
                new OnerilenRestoran
                {
                    Sira = 7,
                    RestoranID = 3
                },
                new OnerilenRestoran
                {
                    Sira = 8,
                    RestoranID = 2
                }
            };

            OnerilenRestoranlar.ForEach(x => context.OnerilenRestoranlar.Add(x));
            context.SaveChanges();
            #endregion

            #region Menuler

            var Menuler = new List<Menu>
            {
                new Menu
                {
                    MenuIsmi = "Pizzalar",
                    SubeID = 1
                },
                new Menu
                {
                    MenuIsmi = "İçecekler",
                    SubeID = 1
                },
                new Menu
                {
                    MenuIsmi = "Ekstra Soslar",
                    SubeID = 1
                }
            };

            Menuler.ForEach(m => context.Menuler.Add(m));
            context.SaveChanges();
            #endregion

            #region Urunler

            var Urunler = new List<Urun>
            {
                /* Menuler */
                new Urun
                {
                    MenuID = 1,
                    UrunIsmi = "Konyalım Pizza Menü",
                    UrunIcerik = "Pizza(Büyük) + Patates Kızartması (Büyük) + Coca Cola (Büyük)",
                    Fiyat = 17.50f
                },
                new Urun
                {
                    MenuID = 1,
                    UrunIsmi = "Bol Malzemos Pizza Menü",
                    UrunIcerik = "Pizza(Büyük) + Patates Kızartması (Büyük) + Coca Cola (Büyük)",
                    Fiyat = 28.50f
                },
                new Urun
                {
                    MenuID = 1,
                    UrunIsmi = "Adanalım Pizza Menü",
                    UrunIcerik = "Pizza(Büyük) + Patates Kızartması (Büyük) + Coca Cola (Büyük)",
                    Fiyat = 30.00f
                },
                /* icecekler */
                /* Menuler */
                new Urun
                {
                    MenuID = 2,
                    UrunIsmi = "Coca Cola",
                    UrunIcerik = "Büyük",
                    Fiyat = 2.50f
                },
                new Urun
                {
                    MenuID = 2,
                    UrunIsmi = "Coca Cola Light",
                    UrunIcerik = "Büyük",
                    Fiyat = 2.50f
                },
                new Urun
                {
                    MenuID = 2,
                    UrunIsmi = "Ice Tea",
                    UrunIcerik = "Büyük",
                    Fiyat = 2.50f
                },
                /* ekstra soslar */
                new Urun
                {
                    MenuID = 3,
                    UrunIsmi = "Ketçap",
                    Fiyat = 0.25f
                },
                new Urun
                {
                    MenuID = 3,
                    UrunIsmi = "Mayonez",
                    Fiyat = 0.25f
                },
                new Urun
                {
                    MenuID = 3,
                    UrunIsmi = "Hardal",
                    Fiyat = 0.25f
                },
                new Urun
                {
                    MenuID = 3,
                    UrunIsmi = "Barbekü",
                    Fiyat = 0.25f
                },
            };

            Urunler.ForEach(u => context.Urunler.Add(u));
            context.SaveChanges();

            #endregion

            #region Yapilandirma

            /* admin kullanicisi ekleme */
            var userStore = new UserStore<ApplicationUser>(applicationDbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            ApplicationUser admin = new ApplicationUser
            {
                UserName = "admin@yemeksepette.com",
                Email = "admin@yemeksepette.com"
            };
            ApplicationUser deneme = new ApplicationUser
            {
                UserName = "deneme@gmail.com",
                Email = "deneme@gmail.com"
            };
            applicationDbContext.Users.Add(admin);
            applicationDbContext.Users.Add(deneme);
            applicationDbContext.SaveChanges();

            userManager.AddPassword(admin.Id, "123456");
            userManager.AddPassword(deneme.Id, "123456");

            /* rolleri ve admin kullanicisina admin rolunu ekleme */
            var rolStore = new RoleStore<IdentityRole>(applicationDbContext);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Kullanıcı"));
            roleManager.Create(new IdentityRole("Restoran"));
            roleManager.Create(new IdentityRole("Şube"));

            /* admin rolunu verme */
            userManager.AddToRole(admin.Id, "Admin");
            userManager.AddToRole(deneme.Id, "Kullanıcı");

            #endregion
        }

    }
}