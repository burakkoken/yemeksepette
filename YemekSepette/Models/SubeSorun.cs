﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class SubeSorun
    {
        public int SubeSorunID { get; set; }

        public int MusteriID { get; set; }

        public int SubeID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "SorunIcerik")]
        public string SorunIcerik { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Tarih")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Tarih { get; set; }

        public virtual Sube Sube { get; set; }

        public virtual Musteri Musteri { get; set; }
    }
}