﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Semt
    {
        public int SemtID { get; set; }

        public int SehirID { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "BolgeAdi")]
        public string SemtAd { get; set; }

        public virtual Sehir Sehir { get; set; }

        public virtual ICollection<Sube> Subeler { get; set; }
    }
}