﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Gorus
    {
        public int GorusID { get; set; }

        [Display(Name = "İçerik")]
        public string GorusIcerik { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Tarih")]
        public DateTime GorusTarih { get; set; }

    }
}