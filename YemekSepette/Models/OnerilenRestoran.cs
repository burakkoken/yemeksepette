﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class OnerilenRestoran
    {
        public int OnerilenRestoranID { get; set; }

        public int RestoranID { get; set; }

        [Display(Name = "Sıra")]
        public int Sira { get; set; }

        public virtual Restoran Restoran { get; set; }

    }
}