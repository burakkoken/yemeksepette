﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Musteri
    {

        public int MusteriID { get; set; }

        public int SehirID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "AdSoyad")]
        public string AdSoyad { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Telefon")]
        public string Telefon { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Adres")]
        public string TamAdres { get; set; }

        public virtual Sehir Sehir { get; set; }

        public virtual ICollection<Siparis> Siparisler { get; set; }

    }
}