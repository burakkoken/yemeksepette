﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Urun
    {
        public int UrunID { get; set; }

        public int MenuID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "UrunAdi")]
        public string UrunIsmi { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Urunİcerik")]
        public string UrunIcerik { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Fiyat")]
        public float Fiyat { get; set; }

        public virtual Menu Menu { get; set; }

    }
}