﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Menu
    {
        public int MenuID { get; set; }

        public int SubeID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "MenüAdi")]
        public string MenuIsmi { get; set; }

        public virtual ICollection<Urun> Urunler { get; set; }

        public virtual Sube Sube { get; set; }
    }
}