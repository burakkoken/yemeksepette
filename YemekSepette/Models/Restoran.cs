﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Restoran
    {
        public int RestoranID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "RestoranAdi")]
        public string RestoranIsmi { get; set; }
        [Display(Name = "Logo")]
        public string RestoranLogo { get; set; }

        public virtual ICollection<Sube> Subeler { get; set; }

    }
}