﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Sube
    {
        public int SubeID { get; set; }

        public int RestoranID { get; set; }

        public int SemtID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "SubeAdi")]
        public string SubeIsmi { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Adres")]
        public string AcikAdres { get; set; }

        /* restoran telefonu */
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Telefon")]
        public string Telefon { get; set; }

        /* restoranin calisma baslangic saati */
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBaslangicSaati { get; set; }

        /* restoranin calisma bitis saati */
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBitisSaati { get; set; }

        /* ortalama servis suresi (dakika biciminde) */
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ServisSuresi")]
        public int ServisSuresi { get; set; }

        public virtual Restoran Restoran { get; set; }

        public virtual Semt Semt { get; set; }

        public virtual ICollection<SubeYorum> SubeYorumlari { get; set; }

        public virtual ICollection<Siparis> Siparisler { get; set; }

    }
}