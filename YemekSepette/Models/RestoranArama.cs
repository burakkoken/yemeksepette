﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class RestoranArama
    {

        [Required(ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "Gerekli")]
        public int SehirID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "Gerekli")]
        public int SemtID { get; set; }

        //[Required(ErrorMessage = "Arama içeriği boş olamaz")]
        public string Icerik { get; set; }

    }
}