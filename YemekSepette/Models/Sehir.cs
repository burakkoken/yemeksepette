﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Sehir
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "PlakaNo")]
        public int SehirID { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "SehirAdi")]
        public string SehirAd { get; set; }

        public virtual ICollection<Semt> Semtler { get; set; }
    }
}