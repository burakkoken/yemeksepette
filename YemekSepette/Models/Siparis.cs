﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Models
{
    public class Siparis
    {
        public int SiparisID { get; set; }

        public int UrunID { get; set; }

        public int MusteriID { get; set; }

        public int SubeID { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Adet")]
        public int Adet { get; set; }

        public bool OnaylanmisMi { get; set; }

        public bool IletildiMi { get; set; }

        public virtual Urun Urun { get; set; }

        public virtual Musteri Musteri { get; set; }

        public virtual Sube Sube { get; set; }
    }
}