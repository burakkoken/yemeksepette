﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class SubeSorunController : SubePanelController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        // GET: SubePanel/SubeSorun
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            var Sorunlar = db.SubeSorunlari
                .Where(x => x.SubeID == kullaniciBilgi.SubeID).ToList();
            return View(Sorunlar);
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}