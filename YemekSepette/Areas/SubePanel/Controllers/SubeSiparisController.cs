﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class SubeSiparisController : SubePanelController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        // GET: SubePanel/SubeSiparis
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            var Siparisler = db.Siparisler
                .Where(x => x.SubeID == kullaniciBilgi.SubeID && x.IletildiMi == false).ToList().GroupBy(x => x.MusteriID);
            return View(Siparisler);
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Siparis(int? Id)
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            var Siparis = db.Siparisler
                .Where(x => x.SubeID == kullaniciBilgi.SubeID && x.IletildiMi == false && x.MusteriID == Id).ToList();
            ViewBag.MusteriAdi = db.Musteriler.Where(x => x.MusteriID == Id).FirstOrDefault().AdSoyad;
            ViewBag.MusteriAdres = db.Musteriler.Where(x => x.MusteriID == Id).FirstOrDefault().TamAdres; float toplamTutar = Siparis.Sum(x => x.Urun.Fiyat * x.Adet);
            ViewBag.Tutar = toplamTutar;
            ViewBag.MusteriID = Siparis.First().MusteriID;
            return View(Siparis);
        }

        public ActionResult SiparisIlet(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var Geciciler = db.Siparisler.Where(x => x.MusteriID == id && x.IletildiMi == false).ToList();
            Geciciler.ForEach(s => s.IletildiMi = true);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}