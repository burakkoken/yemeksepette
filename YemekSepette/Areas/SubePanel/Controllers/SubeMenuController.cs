﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class SubeMenuController : SubePanelController
    {
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: SubePanel/SubeMenu
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            var menuler = db.Menuler.Include(m => m.Sube).Where(x => x.SubeID == kullaniciBilgi.SubeID);
            return View(menuler.ToList());
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: SubePanel/SubeMenu/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubePanel/SubeMenu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MenuID,SubeID,MenuIsmi")] Menu menu)
        {
            var Kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var KullaniciBilgi = db.Subeler.Where(x => x.SubeID == Kullanici.KullaniciTipId).First();

            if (ModelState.IsValid)
            {

                menu.SubeID = KullaniciBilgi.SubeID;
                db.Menuler.Add(menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(menu);
        }

        // GET: SubePanel/SubeMenu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menuler.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }

            return View(menu);
        }

        // POST: SubePanel/SubeMenu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MenuID,SubeID,MenuIsmi")] Menu menu)
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();

            if (ModelState.IsValid)
            {
                menu.SubeID = kullaniciBilgi.SubeID;
                db.Entry(menu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(menu);
        }

        // GET: SubePanel/SubeMenu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menuler.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        // POST: SubePanel/SubeMenu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var siparisler = db.Siparisler.Where(x => x.Urun.MenuID == id).ToList();
            siparisler.ForEach(x => db.Siparisler.Remove(x));
            var urunler = db.Urunler.Where(x => x.MenuID == id).ToList();
            urunler.ForEach(u => db.Urunler.Remove(u));
            Menu menu = db.Menuler.Find(id);
            db.Menuler.Remove(menu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}