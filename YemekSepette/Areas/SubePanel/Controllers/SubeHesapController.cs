﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class SubeHesapController : SubePanelController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: SubePanel/SubeHesap
        public ActionResult Index(YemekSepette.Controllers.ManageController.ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordSuccess ? Resources.Kullanici.Home.ParolaDegisimBasarili
                : message == YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordFailed ? "Parola Değiştirilemedi"
                : "";
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            Sube sube = db.Subeler.Find(kullaniciBilgi.SubeID);
            return View(sube);
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public ActionResult Edit()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            Sube sube = db.Subeler.Find(kullaniciBilgi.SubeID);
            SubeDuzenle yeni = new SubeDuzenle
            {
                eMail = kullanici.Email,
                AcikAdres = sube.AcikAdres,
                CalismaBaslangicSaati = sube.CalismaBaslangicSaati,
                CalismaBitisSaati = sube.CalismaBitisSaati,
                SehirId = sube.Semt.SehirID,
                SemtAdi = sube.Semt.SemtAd,
                ServisSuresi = sube.ServisSuresi,
                SubeIsmi = sube.SubeIsmi,
                Telefon = sube.Telefon
            };
            ViewBag.SehirListesi = db.Sehirler;
            return View(yeni);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubeDuzenle sube)
        {
            if (ModelState.IsValid)
            {
                var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
                Semt yeniSemt;
                if (db.Semtler.Where(x => x.SemtAd == sube.SemtAdi && x.SehirID == sube.SehirId).Count() == 0)
                {

                    yeniSemt = new Semt { SehirID = sube.SehirId, SemtAd = sube.SemtAdi };
                    db.Semtler.Add(yeniSemt);
                }
                else
                {
                    yeniSemt = db.Semtler.Where(x => x.SemtAd == sube.SemtAdi && x.SehirID == sube.SehirId).First();
                }

                Sube degisenSube = db.Subeler.Where(x => x.SubeID == kullaniciBilgi.SubeID).First();
                degisenSube.AcikAdres = sube.AcikAdres;
                degisenSube.CalismaBaslangicSaati = sube.CalismaBaslangicSaati;
                degisenSube.CalismaBitisSaati = sube.CalismaBitisSaati;
                degisenSube.RestoranID = kullaniciBilgi.RestoranID;
                degisenSube.SemtID = yeniSemt.SemtID;
                degisenSube.ServisSuresi = sube.ServisSuresi;
                degisenSube.SubeIsmi = sube.SubeIsmi;
                degisenSube.Telefon = sube.Telefon;
                db.SaveChanges();
                if (kullanici.UserName != sube.eMail)
                {
                    kullanici.Email = sube.eMail;
                    kullanici.UserName = sube.eMail;
                    applicationDbContext.SaveChanges();
                    return RedirectToAction("LogOff", "Account", new { });
                }
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { message = YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordSuccess });
            }
            return RedirectToAction("Index", new { Message = YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordFailed });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }
    }
}