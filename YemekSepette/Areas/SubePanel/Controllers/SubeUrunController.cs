﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class SubeUrunController : SubePanelController
    {
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: SubePanel/SubeUrun
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            var urunler = db.Urunler.Include(u => u.Menu).Where(x => kullaniciBilgi.SubeID == x.Menu.SubeID);
            return View(urunler.ToList());
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        // GET: SubePanel/SubeUrun/Create
        public ActionResult Create()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            ViewBag.MenuID = new SelectList(db.Menuler.Where(x => x.SubeID == kullaniciBilgi.SubeID), "MenuID", "MenuIsmi");
            return View();
        }

        // POST: SubePanel/SubeUrun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UrunID,MenuID,UrunIsmi,UrunIcerik,Fiyat")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Urunler.Add(urun);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            ViewBag.MenuID = new SelectList(db.Menuler.Where(x => x.SubeID == kullaniciBilgi.SubeID), "MenuID", "MenuIsmi", urun.MenuID);
            return View(urun);
        }

        // GET: SubePanel/SubeUrun/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            ViewBag.MenuID = new SelectList(db.Menuler.Where(x => x.SubeID == kullaniciBilgi.SubeID), "MenuID", "MenuIsmi", urun.MenuID);
            return View(urun);
        }

        // POST: SubePanel/SubeUrun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UrunID,MenuID,UrunIsmi,UrunIcerik,Fiyat")] Urun urun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(urun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Subeler.Where(x => x.SubeID == kullanici.KullaniciTipId).First();
            ViewBag.MenuID = new SelectList(db.Menuler.Where(x => x.SubeID == kullaniciBilgi.SubeID), "MenuID", "MenuIsmi", urun.MenuID);

            return View(urun);
        }

        // GET: SubePanel/SubeUrun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        // POST: SubePanel/SubeUrun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var siparisler = db.Siparisler.Where(x => x.UrunID == id).ToList();
            siparisler.ForEach(x => db.Siparisler.Remove(x));
            Urun urun = db.Urunler.Find(id);
            db.Urunler.Remove(urun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}