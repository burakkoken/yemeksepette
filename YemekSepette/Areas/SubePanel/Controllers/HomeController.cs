﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.SubePanel.Models;

namespace YemekSepette.Areas.SubePanel.Controllers
{
    public class HomeController : SubePanelController
    {
        // GET: SubePanel/Home
        public ActionResult Index()
        {
            return RedirectToAction("Index", "SubeSiparis", new { });
        }
    }
}