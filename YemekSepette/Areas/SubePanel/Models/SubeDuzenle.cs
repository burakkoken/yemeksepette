﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.SubePanel.Models
{
    public class SubeDuzenle
    {
        [EmailAddress]
        [Display(Name = "Email")]
        public string eMail { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Sehir")]
        public int SehirId { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Bolge")]
        public string SemtAdi { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "SubeAdi")]
        public string SubeIsmi { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Adres")]
        public string AcikAdres { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Telefon")]
        public string Telefon { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "CalismaSaatleri")]
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBaslangicSaati { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "CalismaSaatleriDisinda")]
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBitisSaati { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ServisSuresi")]
        public int ServisSuresi { get; set; }
    }
}