﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.Admin.Models;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class RolYonetimController : AdminController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetim
        public ActionResult Index()
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            var model = roleManager.Roles.ToList();

            return View(model);
        }

        public ActionResult RolEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolEkle(RolEkleModel rol)
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            if (roleManager.RoleExists(rol.RolAdi) == false)
            {
                roleManager.Create(new IdentityRole(rol.RolAdi));
            }

            return RedirectToAction("Index");
        }

        public ActionResult RolKullaniciEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolKullaniciEkle(RolKullaniciEkleModel model)
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.KullaniciAdi);

            if (!userManager.IsInRole(kullanici.Id, model.RolAdi))
            {
                userManager.AddToRole(kullanici.Id, model.RolAdi);
            }



            return RedirectToAction("Index");
        }
    }
}