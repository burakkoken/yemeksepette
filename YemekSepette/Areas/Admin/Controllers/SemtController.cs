﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class SemtController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: Admin/Semt
        public ActionResult Index(int? page, string currentFiler, string arama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;

            var semtler = db.Semtler.Include(s => s.Sehir).OrderBy(s => s.SehirID).ToList();

            if (!String.IsNullOrEmpty(arama))
            {
                semtler = semtler.Where(s => s.Sehir.SehirAd.ToUpper().Contains(arama.ToUpper())
                || s.SemtAd.ToUpper().Contains(arama.ToUpper())
                ).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);
            //var semtler = db.Semtler.Include(s => s.Sehir);

            ViewBag.SonucSayisi = semtler.Count();
            return View(semtler.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Semt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semtler.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            return View(semt);
        }

        // GET: Admin/Semt/Create
        public ActionResult Create()
        {
            ViewBag.SehirID = new SelectList(db.Sehirler, "SehirID", "SehirAd");
            return View();
        }

        // POST: Admin/Semt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SemtID,SehirID,SemtAd")] Semt semt)
        {
            if (ModelState.IsValid)
            {
                db.Semtler.Add(semt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SehirID = new SelectList(db.Sehirler, "SehirID", "SehirAd", semt.SehirID);
            return View(semt);
        }

        // GET: Admin/Semt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semtler.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            ViewBag.SehirID = new SelectList(db.Sehirler, "SehirID", "SehirAd", semt.SehirID);
            return View(semt);
        }

        // POST: Admin/Semt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SemtID,SehirID,SemtAd")] Semt semt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(semt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SehirID = new SelectList(db.Sehirler, "SehirID", "SehirAd", semt.SehirID);
            return View(semt);
        }

        // GET: Admin/Semt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semtler.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            return View(semt);
        }

        // POST: Admin/Semt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Semt semt = db.Semtler.Find(id);
            db.Semtler.Remove(semt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}