﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class GorusController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: Admin/Gorus
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.DateSortParam = siralama == "tarih_artan" ? "tarih_azalan" : "tarih_artan";
            ViewBag.CurrentSort = siralama;

            var gorusler = db.Gorusler.OrderByDescending(s => s.GorusTarih).ToList();
            if (siralama == "tarih_artan")
            {
                gorusler = db.Gorusler.OrderBy(s => s.GorusTarih).ToList();
            }

            if (!String.IsNullOrEmpty(arama))
            {
                gorusler = gorusler.Where(s => s.GorusTarih.ToString().Contains(arama.ToUpper())
                || s.GorusIcerik.ToUpper().Contains(arama.ToUpper())
                ).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);

            ViewBag.SonucSayisi = gorusler.Count();
            return View(gorusler.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Gorus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gorus gorus = db.Gorusler.Find(id);
            if (gorus == null)
            {
                return HttpNotFound();
            }
            return View(gorus);
        }

        // GET: Admin/Gorus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Gorus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GorusID,GorusIcerik,GorusTarih")] Gorus gorus)
        {
            if (ModelState.IsValid)
            {
                db.Gorusler.Add(gorus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gorus);
        }

        // GET: Admin/Gorus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gorus gorus = db.Gorusler.Find(id);
            if (gorus == null)
            {
                return HttpNotFound();
            }
            return View(gorus);
        }

        // POST: Admin/Gorus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GorusID,GorusIcerik,GorusTarih")] Gorus gorus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gorus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gorus);
        }

        // GET: Admin/Gorus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gorus gorus = db.Gorusler.Find(id);
            if (gorus == null)
            {
                return HttpNotFound();
            }
            return View(gorus);
        }

        // POST: Admin/Gorus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gorus gorus = db.Gorusler.Find(id);
            db.Gorusler.Remove(gorus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}