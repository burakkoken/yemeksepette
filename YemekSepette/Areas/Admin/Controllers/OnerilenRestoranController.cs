﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class OnerilenRestoranController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: Admin/OnerilenRestoran
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.SortParam = siralama == "azalan" ? "artan" : "azalan";
            ViewBag.CurrentSort = siralama;

            var onerilenRestoranlar = db.OnerilenRestoranlar.Include(o => o.Restoran).OrderBy(s => s.Sira).ToList();
            if (siralama == "azalan")
            {
                onerilenRestoranlar = db.OnerilenRestoranlar.Include(o => o.Restoran).OrderByDescending(s => s.Sira).ToList();
            }

            if (!String.IsNullOrEmpty(arama))
            {
                onerilenRestoranlar = onerilenRestoranlar.Where(s => s.Restoran.RestoranIsmi.ToUpper().Contains(arama.ToUpper())).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);
            //var onerilenRestoranlar = db.OnerilenRestoranlar.Include(o => o.Restoran);

            ViewBag.SonucSayisi = onerilenRestoranlar.Count();
            return View(onerilenRestoranlar.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/OnerilenRestoran/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OnerilenRestoran onerilenRestoran = db.OnerilenRestoranlar.Find(id);
            if (onerilenRestoran == null)
            {
                return HttpNotFound();
            }
            return View(onerilenRestoran);
        }

        // GET: Admin/OnerilenRestoran/Create
        public ActionResult Create()
        {
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi");
            return View();
        }

        // POST: Admin/OnerilenRestoran/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OnerilenRestoranID,RestoranID,Sira")] OnerilenRestoran onerilenRestoran)
        {
            if (ModelState.IsValid)
            {
                db.OnerilenRestoranlar.Add(onerilenRestoran);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", onerilenRestoran.RestoranID);
            return View(onerilenRestoran);
        }

        // GET: Admin/OnerilenRestoran/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OnerilenRestoran onerilenRestoran = db.OnerilenRestoranlar.Find(id);
            if (onerilenRestoran == null)
            {
                return HttpNotFound();
            }
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", onerilenRestoran.RestoranID);
            return View(onerilenRestoran);
        }

        // POST: Admin/OnerilenRestoran/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OnerilenRestoranID,RestoranID,Sira")] OnerilenRestoran onerilenRestoran)
        {
            if (ModelState.IsValid)
            {
                db.Entry(onerilenRestoran).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", onerilenRestoran.RestoranID);
            return View(onerilenRestoran);
        }

        // GET: Admin/OnerilenRestoran/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OnerilenRestoran onerilenRestoran = db.OnerilenRestoranlar.Find(id);
            if (onerilenRestoran == null)
            {
                return HttpNotFound();
            }
            return View(onerilenRestoran);
        }

        // POST: Admin/OnerilenRestoran/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OnerilenRestoran onerilenRestoran = db.OnerilenRestoranlar.Find(id);
            db.OnerilenRestoranlar.Remove(onerilenRestoran);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}