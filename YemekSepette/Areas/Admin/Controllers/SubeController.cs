﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class SubeController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: Admin/Sube
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.SortParam = siralama == "azalan" ? "artan" : "azalan";
            ViewBag.CurrentSort = siralama;

            var subeler = db.Subeler.Include(s => s.Restoran).Include(s => s.Semt).OrderBy(x => x.Restoran.RestoranIsmi).ToList();
            if (siralama == "azalan")
            {
                subeler = db.Subeler.Include(s => s.Restoran).Include(s => s.Semt).OrderByDescending(x => x.Restoran.RestoranIsmi).ToList();
            }

            if (!String.IsNullOrEmpty(arama))
            {
                subeler = subeler.Where(x => x.Restoran.RestoranIsmi.ToUpper().Contains(arama.ToUpper())
                || x.SubeIsmi.ToUpper().Contains(arama.ToUpper()) || x.Semt.SemtAd.ToUpper().Contains(arama.ToUpper())
                || x.Semt.Sehir.SehirAd.ToUpper().Contains(arama.ToUpper())
                ).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);

            ViewBag.SonucSayisi = subeler.Count();
            //var subeler = db.Subeler.Include(s => s.Restoran).Include(s => s.Semt);
            return View(subeler.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Sube/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sube sube = db.Subeler.Find(id);
            if (sube == null)
            {
                return HttpNotFound();
            }
            return View(sube);
        }

        // GET: Admin/Sube/Create
        public ActionResult Create()
        {
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi");
            ViewBag.SemtID = new SelectList(db.Semtler, "SemtID", "SemtAd");
            return View();
        }

        // POST: Admin/Sube/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SubeID,RestoranID,SemtID,SubeIsmi,AcikAdres,Telefon,ServisSuresi")] Sube sube)
        {
            if (ModelState.IsValid)
            {
                db.Subeler.Add(sube);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", sube.RestoranID);
            ViewBag.SemtID = new SelectList(db.Semtler, "SemtID", "SemtAd", sube.SemtID);
            return View(sube);
        }

        // GET: Admin/Sube/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sube sube = db.Subeler.Find(id);
            if (sube == null)
            {
                return HttpNotFound();
            }
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", sube.RestoranID);
            ViewBag.SemtID = new SelectList(db.Semtler, "SemtID", "SemtAd", sube.SemtID);
            return View(sube);
        }

        // POST: Admin/Sube/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubeID,RestoranID,SemtID,SubeIsmi,AcikAdres,Telefon,ServisSuresi")] Sube sube)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sube).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RestoranID = new SelectList(db.Restoranlar, "RestoranID", "RestoranIsmi", sube.RestoranID);
            ViewBag.SemtID = new SelectList(db.Semtler, "SemtID", "SemtAd", sube.SemtID);
            return View(sube);
        }

        // GET: Admin/Sube/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sube sube = db.Subeler.Find(id);
            if (sube == null)
            {
                return HttpNotFound();
            }
            return View(sube);
        }

        // POST: Admin/Sube/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sube sube = db.Subeler.Find(id);
            db.Subeler.Remove(sube);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}