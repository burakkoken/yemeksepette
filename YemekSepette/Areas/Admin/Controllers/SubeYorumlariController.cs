﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class SubeYorumlariController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();

        // GET: Admin/SubeYorumlari
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.DateSortParam = siralama == "tarih_artan" ? "tarih_azalan" : "tarih_artan";
            ViewBag.CurrentSort = siralama;

            var yorumlar = db.SubeYorumlari.Include(x => x.Musteri).Include(x => x.Sube).OrderByDescending(x => x.Tarih).ToList();
            if (siralama == "tarih_artan")
            {
                yorumlar = db.SubeYorumlari.Include(x => x.Musteri).Include(x => x.Sube).OrderBy(x => x.Tarih).ToList();
            }

            if (!String.IsNullOrEmpty(arama))
            {
                yorumlar = yorumlar.Where(x => x.Sube.SubeIsmi.ToUpper().Contains(arama.ToUpper())
                || x.Tarih.ToString().Contains(arama.ToUpper()) || x.YorumIcerik.ToUpper().Contains(arama.ToUpper())
                ).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);

            ViewBag.SonucSayisi = yorumlar.Count();

            //var subeYorumlari = db.SubeYorumlari.Include(s => s.Musteri).Include(s => s.Sube);
            return View(yorumlar.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/SubeYorumlari/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubeYorum subeYorum = db.SubeYorumlari.Find(id);
            if (subeYorum == null)
            {
                return HttpNotFound();
            }
            return View(subeYorum);
        }

        // GET: Admin/SubeYorumlari/Create
        public ActionResult Create()
        {
            ViewBag.MusteriID = new SelectList(db.Musteriler, "MusteriID", "AdSoyad");
            ViewBag.SubeID = new SelectList(db.Subeler, "SubeID", "SubeIsmi");
            return View();
        }

        // POST: Admin/SubeYorumlari/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SubeYorumID,MusteriID,SubeID,YorumIcerik,Tarih")] SubeYorum subeYorum)
        {
            if (ModelState.IsValid)
            {
                db.SubeYorumlari.Add(subeYorum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MusteriID = new SelectList(db.Musteriler, "MusteriID", "AdSoyad", subeYorum.MusteriID);
            ViewBag.SubeID = new SelectList(db.Subeler, "SubeID", "SubeIsmi", subeYorum.SubeID);
            return View(subeYorum);
        }

        // GET: Admin/SubeYorumlari/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubeYorum subeYorum = db.SubeYorumlari.Find(id);
            if (subeYorum == null)
            {
                return HttpNotFound();
            }
            ViewBag.MusteriID = new SelectList(db.Musteriler, "MusteriID", "AdSoyad", subeYorum.MusteriID);
            ViewBag.SubeID = new SelectList(db.Subeler, "SubeID", "SubeIsmi", subeYorum.SubeID);
            return View(subeYorum);
        }

        // POST: Admin/SubeYorumlari/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubeYorumID,MusteriID,SubeID,YorumIcerik,Tarih")] SubeYorum subeYorum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subeYorum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MusteriID = new SelectList(db.Musteriler, "MusteriID", "AdSoyad", subeYorum.MusteriID);
            ViewBag.SubeID = new SelectList(db.Subeler, "SubeID", "SubeIsmi", subeYorum.SubeID);
            return View(subeYorum);
        }

        // GET: Admin/SubeYorumlari/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubeYorum subeYorum = db.SubeYorumlari.Find(id);
            if (subeYorum == null)
            {
                return HttpNotFound();
            }
            return View(subeYorum);
        }

        // POST: Admin/SubeYorumlari/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubeYorum subeYorum = db.SubeYorumlari.Find(id);
            db.SubeYorumlari.Remove(subeYorum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}