﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.Admin.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class RestoranController : AdminController
    {
        private YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        // GET: Admin/Restoran
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.SortParam = siralama == "azalan" ? "artan" : "azalan";
            ViewBag.CurrentSort = siralama;

            var restoranlar = db.Restoranlar.OrderBy(x => x.RestoranIsmi).ToList();
            if (siralama == "azalan")
            {
                restoranlar = db.Restoranlar.OrderByDescending(x => x.RestoranIsmi).ToList();
            }

            if (!String.IsNullOrEmpty(arama))
            {
                restoranlar = restoranlar.Where(x => x.RestoranIsmi.ToUpper().Contains(arama.ToUpper())).ToList();
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);

            ViewBag.SonucSayisi = restoranlar.Count();

            return View(restoranlar.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Restoran/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Restoran restoran = db.Restoranlar.Find(id);
            if (restoran == null)
            {
                return HttpNotFound();
            }
            return View(restoran);
        }

        // GET: Admin/Restoran/Create
        public ActionResult Create()
        {
            return View();
        }

        const string imageFolderPath = "/Content/img/restaurant/";
        // POST: Admin/Restoran/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RestoranEkleModel restoran)
        {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;

                if (restoran.RestoranLogo != null && restoran.RestoranLogo.ContentLength > 0)
                {
                    fileName = restoran.RestoranLogo.FileName;
                    var path = Path.Combine(Server.MapPath(imageFolderPath), fileName);
                    /* eger varsa sil */
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    restoran.RestoranLogo.SaveAs(path);
                }

                /* restoran bilgilerini kaydet */
                Restoran yeni = new Restoran();
                yeni.RestoranIsmi = restoran.RestoranIsmi;
                yeni.RestoranLogo = imageFolderPath + fileName;
                db.Restoranlar.Add(yeni);
                db.SaveChanges();

                /* restoran hesabini ekle */
                var user = new ApplicationUser { UserName = restoran.Email, Email = restoran.Email, KullaniciTipId = yeni.RestoranID };

                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);

                applicationDbContext.Users.Add(user);
                applicationDbContext.SaveChanges();

                /* parola ata */
                userManager.AddPassword(user.Id, restoran.Password);
                /* rol ata */
                userManager.AddToRole(user.Id, "Restoran");




                //db.Restoranlar.Add(restoran);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(restoran);
        }

        // GET: Admin/Restoran/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Restoran restoran = db.Restoranlar.Find(id);
            if (restoran == null)
            {
                return HttpNotFound();
            }
            return View(restoran);
        }

        // POST: Admin/Restoran/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RestoranID,RestoranIsmi")] Restoran restoran)
        {
            if (ModelState.IsValid)
            {
                db.Entry(restoran).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(restoran);
        }

        // GET: Admin/Restoran/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Restoran restoran = db.Restoranlar.Find(id);
            if (restoran == null)
            {
                return HttpNotFound();
            }
            return View(restoran);
        }

        // POST: Admin/Restoran/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Restoran restoran = db.Restoranlar.Find(id);
            db.Restoranlar.Remove(restoran);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}