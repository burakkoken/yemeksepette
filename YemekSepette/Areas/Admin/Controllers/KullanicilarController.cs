﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.Admin.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class KullanicilarController : AdminController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        YemekSepetteContext db = new YemekSepetteContext();


        // GET: Admin/Kullanicilar
        public ActionResult Index(int? page, string currentFiler, string arama, string siralama)
        {
            var rolStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            /* */
            if (arama != null)
            {
                page = 1;
            }
            else
            {
                arama = currentFiler;
            }
            ViewBag.CurrentFilter = arama;
            ViewBag.SortParam = siralama == "azalan" ? "artan" : "azalan";
            ViewBag.CurrentSort = siralama;

            ICollection<ApplicationUser> kullanicilar = userManager.Users.ToList();
            var hesaplar = kullanicilar.Select(x =>
                           new Hesap
                           {
                               Id = x.Id,
                               Mail = x.Email,
                               Rol = userManager.GetRoles(x.Id).First()
                           }
                        ).OrderBy(x => x.Rol);

            if (siralama == "azalan")
            {
                hesaplar = hesaplar.OrderByDescending(x => x.Rol);
            }

            if (!String.IsNullOrEmpty(arama))
            {
                if (siralama == "azalan")
                {
                    hesaplar = hesaplar.Where(x => x.Mail.ToUpper().Contains(arama.ToUpper())
                        || x.Rol.ToUpper().Contains(arama.ToUpper())
                    ).OrderByDescending(x => x.Rol);
                }
                else
                {
                    hesaplar = hesaplar.Where(x => x.Mail.ToUpper().Contains(arama.ToUpper())
                        || x.Rol.ToUpper().Contains(arama.ToUpper())
                    ).OrderBy(x => x.Rol);
                }

            }


            ViewBag.SonucSayisi = hesaplar.Count();
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(hesaplar.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult ParolaDegistir(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index");
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            if (userManager.FindById(id) == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.HesapId = id;
            return View();
        }

        [HttpPost]
        public ActionResult ParolaDegistir(ParolaDegisModel model, string id)
        {

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (userManager.FindById(id) == null)
            {
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                userManager.RemovePassword(id);
                userManager.AddPassword(id, model.NewPassword);
                return RedirectToAction("Index");

            }


            ViewBag.HesapId = id;
            return View(model);
        }

        public ActionResult HesapSil(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index");
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            if (userManager.FindById(id) == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.HesapId = id;
            return View();
        }

        [HttpPost]
        public ActionResult HesapSilmeyiTamamla(string id)
        {

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (userManager.FindById(id) == null)
            {
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {

                var kullaniciTipId = context.Users.Where(x => x.Id == id).First().KullaniciTipId;

                if (userManager.IsInRole(id, "Şube"))
                {

                    var siparisler = db.Siparisler.Where(x => x.SubeID == kullaniciTipId).ToList();
                    siparisler.ForEach(x => db.Siparisler.Remove(x));
                    var urunler = db.Urunler.Where(x => x.Menu.SubeID == kullaniciTipId).ToList();
                    urunler.ForEach(x => db.Urunler.Remove(x));
                    var menuler = db.Menuler.Where(x => x.SubeID == kullaniciTipId).ToList();
                    menuler.ForEach(x => db.Menuler.Remove(x));
                    var sorunlar = db.SubeSorunlari.Where(x => x.SubeID == kullaniciTipId).ToList();
                    sorunlar.ForEach(x => db.SubeSorunlari.Remove(x));
                    var yorumlar = db.SubeYorumlari.Where(x => x.SubeID == kullaniciTipId).ToList();
                    yorumlar.ForEach(x => db.SubeYorumlari.Remove(x));

                    /* semtte baska sube var mi? */
                    var sube = db.Subeler.Where(x => x.SubeID == kullaniciTipId).First();
                    var semtKontrol = db.Subeler.Where(x => x.SemtID == sube.SemtID).ToList();

                    if (semtKontrol.Count() == 1)
                    {
                        db.Semtler.Remove(db.Semtler.Where(x => x.SemtID == sube.SemtID).First());
                    }
                    // en sonda subelerden
                    db.Subeler.Remove(sube);

                }
                else if (userManager.IsInRole(id, "Restoran"))
                {
                    var subeleri = db.Subeler.Where(x => x.RestoranID == kullaniciTipId).ToList();

                    foreach (var sube in subeleri)
                    {

                        var siparisler = db.Siparisler.Where(x => x.SubeID == sube.SubeID).ToList();
                        siparisler.ForEach(x => db.Siparisler.Remove(x));
                        var urunler = db.Urunler.Where(x => x.Menu.SubeID == sube.SubeID).ToList();
                        urunler.ForEach(x => db.Urunler.Remove(x));
                        var menuler = db.Menuler.Where(x => x.SubeID == sube.SubeID).ToList();
                        menuler.ForEach(x => db.Menuler.Remove(x));
                        var sorunlar = db.SubeSorunlari.Where(x => x.SubeID == sube.SubeID).ToList();
                        sorunlar.ForEach(x => db.SubeSorunlari.Remove(x));
                        var yorumlar = db.SubeYorumlari.Where(x => x.SubeID == sube.SubeID).ToList();
                        yorumlar.ForEach(x => db.SubeYorumlari.Remove(x));

                        /* semtte baska sube var mi? */
                        var semtKontrol = db.Subeler.Where(x => x.SemtID == sube.SemtID).ToList();
                        if (semtKontrol.Count() == 1)
                        {
                            db.Semtler.Remove(db.Semtler.Where(x => x.SemtID == sube.SemtID).First());
                        }
                        // en sonda subelerden
                        db.Subeler.Remove(sube);
                    }

                    /* onerilen restoranlardan kaldir */
                    var onerilenRestoranlar = db.OnerilenRestoranlar.Where(x => x.RestoranID == kullaniciTipId).ToList();
                    onerilenRestoranlar.ForEach(x => db.OnerilenRestoranlar.Remove(x));

                    /* restoran bilgisini al */
                    var restoran = db.Restoranlar.Where(x => x.RestoranID == kullaniciTipId).First();
                    // en sonda restoranlardan
                    db.Restoranlar.Remove(restoran);
                }
                else if (userManager.IsInRole(id, "Kullanıcı"))
                {
                    var siparisler = db.Siparisler.Where(x => x.MusteriID == kullaniciTipId).ToList();
                    siparisler.ForEach(x => db.Siparisler.Remove(x));

                }

                db.SaveChanges();
                context.Users.Remove(userManager.FindById(id));
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
    }
}