﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YemekSepette.Areas.Admin.Controllers
{
    public class HomeController : AdminController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Kullanicilar");
        }
    }
}