﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.Admin.Models
{
    public class RestoranEkleModel
    {
        [Required]
        [Display(Name = "Restoran İsmi")]
        public string RestoranIsmi { get; set; }

        public HttpPostedFileBase RestoranLogo { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "ParolaUzunlukKontrol", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Parola")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ParolaTekrari")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "ParolaEslesmedi")]
        public string ConfirmPassword { get; set; }
    }
}