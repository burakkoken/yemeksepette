﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.Admin.Models
{
    public class ParolaDegisModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "YeniParola")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ParolaTekrari2")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "ParolaEslesmedi")]
        public string ConfirmPassword { get; set; }
    }
}