﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.Admin.Models
{
    public class Hesap
    {
        public string Id { get; set; }

        public string Mail { get; set; }

        public string Rol { get; set; }
    }
}