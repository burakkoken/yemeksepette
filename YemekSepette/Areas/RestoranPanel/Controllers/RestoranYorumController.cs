﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.RestoranPanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.RestoranPanel.Controllers
{
    public class RestoranYorumController : RestoranPanelController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        // GET: RestoranPanel/RestoranYorum
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            var Yorumlar = db.SubeYorumlari
                .Where(x => x.Sube.RestoranID == kullaniciBilgi.RestoranID).ToList();
            return View(Yorumlar);
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}