﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.RestoranPanel.Models;

namespace YemekSepette.Areas.RestoranPanel.Controllers
{
    public class HomeController : RestoranPanelController
    {
        // GET: RestoranPanel/Home
        public ActionResult Index()
        {
            return RedirectToAction("Index", "RestoranSube", new { });
        }
    }
}