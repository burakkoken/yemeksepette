﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.RestoranPanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.RestoranPanel.Controllers
{
    public class RestoranSorunController : RestoranPanelController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        // GET: RestoranPanel/RestoranSorun
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            var Sorunlar = db.SubeSorunlari
                .Where(x => x.Sube.RestoranID == kullaniciBilgi.RestoranID).ToList();
            return View(Sorunlar);
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}