﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.RestoranPanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.RestoranPanel.Controllers
{
    public class RestoranSubeController : RestoranPanelController
    {
        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        // GET: RestoranPanel/RestoranSube
        public ActionResult Index()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            var subeler = db.Subeler
                .Where(x => x.RestoranID == kullaniciBilgi.RestoranID).ToList();
            return View(subeler);
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: RestoranPanel/RestoranSube/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sube sube = db.Subeler.Find(id);
            if (sube == null)
            {
                return HttpNotFound();
            }
            return View(sube);
        }

        // GET: RestoranPanel/RestoranSube/Create
        public ActionResult Create()
        {
            ViewBag.SehirListesi = db.Sehirler;
            return View();
        }

        // POST: RestoranPanel/RestoranSube/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubeEkle yeniSube)
        {
            if (ModelState.IsValid)
            {
                var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
                Semt yeniSemt;
                if (db.Semtler.Where(x => x.SemtAd == yeniSube.SemtAdi && x.SehirID == yeniSube.SehirId).Count() == 0)
                {

                    yeniSemt = new Semt { SehirID = yeniSube.SehirId, SemtAd = yeniSube.SemtAdi };
                    db.Semtler.Add(yeniSemt);
                }
                else
                {
                    yeniSemt = db.Semtler.Where(x => x.SemtAd == yeniSube.SemtAdi && x.SehirID == yeniSube.SehirId).First();
                }

                Sube yeni = new Sube
                {
                    AcikAdres = yeniSube.AcikAdres,
                    CalismaBaslangicSaati = yeniSube.CalismaBaslangicSaati,
                    CalismaBitisSaati = yeniSube.CalismaBitisSaati,
                    RestoranID = kullaniciBilgi.RestoranID,
                    SemtID = yeniSemt.SemtID,
                    ServisSuresi = yeniSube.ServisSuresi,
                    SubeIsmi = yeniSube.SubeIsmi,
                    Telefon = yeniSube.Telefon
                };
                db.Subeler.Add(yeni);
                db.SaveChanges();

                var user = new ApplicationUser { UserName = yeniSube.Email, Email = yeniSube.Email, KullaniciTipId = yeni.SubeID };
                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var userManager = new UserManager<ApplicationUser>(userStore);
                applicationDbContext.Users.Add(user);
                applicationDbContext.SaveChanges();

                userManager.AddPassword(user.Id, yeniSube.Password);
                userManager.AddToRole(user.Id, "Şube");
                return RedirectToAction("Index");
            }
            ViewBag.SehirListesi = db.Sehirler;
            return View(yeniSube);

        }

        // GET: RestoranPanel/RestoranSube/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sube sube = db.Subeler.Find(id);
            if (sube == null)
            {
                return HttpNotFound();
            }
            return View(sube);
        }

        // POST: RestoranPanel/RestoranSube/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var userStore = new UserStore<ApplicationUser>(applicationDbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullaniciListesi = userManager.Users.ToList();
            kullaniciListesi = kullaniciListesi.Where(x => userManager.IsInRole(x.Id, "Şube")).ToList();
            var hesap = kullaniciListesi.Where(x => x.KullaniciTipId == id).First();

            if (hesap == null)
            {
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                applicationDbContext.Users.Remove(userManager.FindById(hesap.Id));
                applicationDbContext.SaveChanges();

                var sorunlar = db.SubeSorunlari.Where(x => x.SubeID == id).ToList();
                sorunlar.ForEach(s => db.SubeSorunlari.Remove(s));
                var yorumlar = db.SubeYorumlari.Where(x => x.SubeID == id).ToList();
                yorumlar.ForEach(s => db.SubeYorumlari.Remove(s));
                var siparisler = db.Siparisler.Where(x => x.SubeID == id).ToList();
                siparisler.ForEach(s => db.Siparisler.Remove(s));
                var urunler = db.Urunler.Where(x => x.Menu.SubeID == id).ToList();
                urunler.ForEach(s => db.Urunler.Remove(s));
                var menuler = db.Menuler.Where(x => x.SubeID == id).ToList();
                menuler.ForEach(s => db.Menuler.Remove(s));
                var subeler = db.Subeler.Where(x => x.SubeID == id).ToList();
                subeler.ForEach(s => db.Subeler.Remove(s));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}