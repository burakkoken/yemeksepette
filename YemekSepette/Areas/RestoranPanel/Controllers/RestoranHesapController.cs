﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using YemekSepette.Areas.RestoranPanel.Models;
using YemekSepette.DAL;
using YemekSepette.Models;

namespace YemekSepette.Areas.RestoranPanel.Controllers
{
    public class RestoranHesapController : RestoranPanelController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        YemekSepetteContext db = new YemekSepetteContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: RestoranPanel/RestoranHesap
        public ActionResult Index(YemekSepette.Controllers.ManageController.ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordSuccess ? Resources.Kullanici.Home.ParolaDegisimBasarili
                : message == YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordFailed ? "Parola Değiştirilemedi"
                : "";
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            Restoran restoran = db.Restoranlar.Find(kullaniciBilgi.RestoranID);
            return View(restoran);
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public ActionResult Edit()
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            Restoran restoran = db.Restoranlar.Find(kullaniciBilgi.RestoranID);
            RestoranDuzenle yeni = new RestoranDuzenle
            {
                eMail = kullanici.Email,
                restoranAdi = restoran.RestoranIsmi
            };
            return View(yeni);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RestoranDuzenle restoran)
        {
            if (ModelState.IsValid)
            {
                var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
                Restoran degisenRestoran = db.Restoranlar.Where(x => x.RestoranID == kullaniciBilgi.RestoranID).First();
                degisenRestoran.RestoranIsmi = restoran.restoranAdi;
                db.SaveChanges();
                if (kullanici.UserName != restoran.eMail)
                {
                    kullanici.Email = restoran.eMail;
                    kullanici.UserName = restoran.eMail;
                    applicationDbContext.SaveChanges();
                    return RedirectToAction("LogOff", "Account", new { area = "", language = HttpContext.Response.Cookies["dil"].Value });
                }
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        public ActionResult ChangeLogo()
        {
            return View();
        }

        const string imageFolderPath = "/Content/img/restaurant/";
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeLogo(LogoModel logo)
        {
            var kullanici = applicationDbContext.Users.Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Restoranlar.Where(x => x.RestoranID == kullanici.KullaniciTipId).First();
            if (ModelState.IsValid)
            {

                string fileName = string.Empty;

                if (logo.RestoranLogo != null && logo.RestoranLogo.ContentLength > 0)
                {
                    fileName = logo.RestoranLogo.FileName;
                    var path = Path.Combine(Server.MapPath(imageFolderPath), fileName);
                    /* eger varsa sil */
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    logo.RestoranLogo.SaveAs(path);
                    var restoran = db.Restoranlar.Where(x => x.RestoranID == kullaniciBilgi.RestoranID).First();
                    restoran.RestoranLogo = imageFolderPath + fileName;
                    db.SaveChanges();
                }
            }


            return View();
        }
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { message = YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordSuccess });
            }
            return RedirectToAction("Index", new { Message = YemekSepette.Controllers.ManageController.ManageMessageId.ChangePasswordFailed });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

    }
}