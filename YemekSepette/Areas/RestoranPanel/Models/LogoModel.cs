﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.RestoranPanel.Models
{
    public class LogoModel
    {
        [Display(Name = "Logo")]
        public HttpPostedFileBase RestoranLogo { get; set; }

    }
}