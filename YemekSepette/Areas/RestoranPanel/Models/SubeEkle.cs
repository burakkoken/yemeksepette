﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.RestoranPanel.Models
{
    public class SubeEkle
    {
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "Şifre 6 ile 100 karakter arasında olmalı.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Parola")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ParolaTekrari")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Kullanici.Home), ErrorMessageResourceName = "ParolaEslesmedi")]
        public string ConfirmPassword { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Sehir")]
        public int SehirId { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Bolge")]
        public string SemtAdi { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ParolaTekrari")]
        public string SubeIsmi { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Adres")]
        public string AcikAdres { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "Telefon")]
        public string Telefon { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "CalismaSaatleri")]
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBaslangicSaati { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "CalismaSaatleriDisinda")]
        [DisplayFormat(DataFormatString = "{0: HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CalismaBitisSaati { get; set; }
        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "ServisSuresi")]

        public int ServisSuresi { get; set; }
    }
}