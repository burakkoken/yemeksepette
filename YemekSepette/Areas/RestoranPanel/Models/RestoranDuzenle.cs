﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YemekSepette.Areas.RestoranPanel.Models
{
    public class RestoranDuzenle
    {
        [EmailAddress]
        [Display(Name = "Email")]
        public string eMail { get; set; }

        [Display(ResourceType = typeof(Resources.Kullanici.Home), Name = "RestoranAdi")]
        public string restoranAdi { get; set; }
    }
}